<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Sale;
use App\Product;
use App\Inventory;
use App\SaleItem;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::all();
        
        if(!empty(session('idsales')))
        {
            $idsales = session('idsales');
            return view('transaction.index', compact('product' , 'idsales'));
        }
        else
        {
            return redirect('/home');  
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sales = new Sale; 
        $sales->total_items = 0;    
        $sales->total_price = 0;    
        $sales->discount = 0;    
        $sales->total_amount = 0;    
        $sales->cash = 0;    
        $sales->user_id = Auth::User()->id;    
        $sales->save();
        
        session(['idsales' => $sales->id]);

        return redirect()->route('transaction.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sales = Sale::find($request['salesid']);
        $sales->member_code = $request['member'];
        $sales->total_item = $request['totalitem'];
        $sales->total_price = $request['total'];
        $sales->discount = $request['discount'];
        $sales->pay = $request['pay'];
        $sales->received = $request['received'];
        $sales->update();

        $detail = SalesDetail::where('sales_id', '=', $request['salesid'])->get();
        foreach($detail as $data){
            $product = Product::where('product_code', '=', $data->product_code)->first();
            $product->stock -= $data->total;
            $product->update();
        }
        return Redirect::route('transaction.print');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function listData($id)
    {
        $detail = SaleItem::leftJoin('products', 'products.id', '=', 'sale_items.product_id')
            ->where('sale_items.id', '=', $id)
            ->get();
        $no = 0;
        $data = array();
        $total = 0;
        $total_item = 0;

        foreach($detail as $list)
        {
            $no ++;
            $row = array();
            $row[] = $no;
            $row[] = $list->product_code;
            $row[] = $list->product_name;
            $row[] = "₱ ".format_money($list->price);
            $row[] = "<input type='number' class='form-control' name='total_$list->sales_detail_id' value='$list->total' onChange='changeCount($list->sales_detail_id)'>";
            $row[] = $list->discount."%";
            $row[] = "₱ ".format_money($list->sub_total);
            $row[] = '<div class="btn-group">
                    <a onclick="deleteItem('.$list->sales_detail_id.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>';
            $data[] = $row;

            $total += $list->price * $list->total;
            $total_item += $list->total;
        }

        $data[] = array("<span class='hide total'>$total</span><span class='hide totalitem'>$total_item</span>", "", "", "", "", "", "", "");
        
        $output = array("data" => $data);
        return response()->json($output);
    }
    
    public function addItem(Request $request)
    {
        $product = Product::where('productCode', $request['productCode'])->first();
        $inventory = Inventory::where( 'productId', $product->id )->first();

        $saleItem = new SaleItem;
        $saleItem->sales_id = $request['idsales'];
        $saleItem->product_id = $product->id;
        $saleItem->price = $inventory->price;
        $saleItem->total_qty = 1;
        $saleItem->discount = $inventory->discount;
        $saleItem->subtotal = $inventory->price - ($inventory->discount/100 * $inventory->price);
        $saleItem->save();
    }

    
    public function loadForm($discount, $total, $cash){
        $payment = $total - ($discount / 100 * $total);
        $change = ($cash != 0) ? $cash - $payment : 0;

        $data = array(
            "totalamount" => format_money($payment),
            "cash" => $payment,
            "amountDue" => format_money($payment),
            "paymentInText" => ($payment > 0) ? translateToWords( $payment ) : "PhP",
            "change" => format_money($change),
            "changeInText" => ($payment > 0) ? translateToWords( $payment ) : "PhP"
        );
        return response()->json($data);
    }
}
