<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Illuminate\Support\Facades\Auth::user()->level != 1) return redirect('/pos');
        $data = User::all();
        $roles = Role::all();

        foreach ( $data as $user)
        {
            $role = Role::where('id', $user->level)->first();
            $user->role = $role['description'];
        }
        return view('users.index', ['users' => $data, 'roles' => $roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (\Illuminate\Support\Facades\Auth::user()->level != 1) return redirect('/pos');
        if ( $request->userId !== null ) if ($this->update($request) ) return redirect()->back();

        $user = User::create([
            'name' => $request->fullname,
            'email' => $request->username,
            'password' => Hash::make($request->password),
            'level' => $request->role,
        ]);

        if ( $user ) return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (\Illuminate\Support\Facades\Auth::user()->level != 1) return redirect('/pos');
        $user = User::where('id',$request->userId)->first();

        $user->name = $request->fullname;
        $user->email = $request->username;
        $user->password = Hash::make($request->password);
        $user->level = $request->role;

        return $user->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
