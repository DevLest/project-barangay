<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleItem extends Model
{
    protected $table = 'sale_items';
    protected $fillable = ['sales_id', 'product_id', 'price', 'total_qty', 'discount', 'subtotal', ];
    public $timestamps = true;
}
