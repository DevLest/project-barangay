@extends('layouts.app', ['activePage' => 'category', 'titlePage' => __('Category')])

@section('content')
  <div class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-header card-header-danger color-theme">
              <h4 class="card-title ">Category</h4>
              <p class="card-category"> Here you can manage all category details and updates</p>
            </div>
            
            <div class="card-body">
              <div class="row">
                <div class="col-12 text-right">
                  <a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#categoryAddModal" onclick="clearFields()">Add category</a>
                </div>
              </div>
              <div class="table-responsive">
                <table class="table" id="dataTable">
                  <thead class=" text-primary">
                    <tr>
                      <th>Category Code</th>
                      <th>Title</th>
                      <th>Date Created</th>
                      @if ( auth()->user() )
                        <th class="text-right">
                          Actions
                        </th>
                      @endif
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ( $categories as $category )
                        <tr>
                          <td>{{ $category->id }}</td>
                            <input type="text" hidden id="category-Id{{ $category->id }}" value="{{ $category->id }}">
                        <td>
                            {{ $category->title }}
                            <input type="text" hidden id="title{{ $category->id }}" value="{{ $category->title }}">
                          </td>
                          <td>
                            {{ date('l jS \of F Y h:i:s A',strtotime($category->created_at)) }}
                          </td>
                          @if ( auth()->user() )
                            <td class="td-actions text-right">
                              <button type="button" rel="tooltip" title="" class="btn btn-primary btn-link btn-sm" data-original-title="Edit" data-toggle="modal" data-target="#categoryAddModal" onclick="loadModal('{{$category->id}}')">
                                <i class="material-icons">edit</i>
                                <div class="ripple-container"></div>
                              </button>
                            </td>
                          @endif
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
    </div>
  </div>
  <script>
    function loadModal( categoryId )
    {
        console.log($('#title'+categoryId).val());
      $('#modal-categorytitle').val( $('#title'+categoryId).val() );
      $('#modal-category-Id').val( $('#category-Id'+categoryId).val() );
    }

    function clearFields()
    {
      $('#modal-categorytitle').val("");
      $('#modal-categoryId').val("");
    }
  </script>
@endsection