<div class="modal fade bd-example-modal-lg" id="productAddModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add New Product</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form method="post" action="{{ route('ci.store') }}" class="form-horizontal">
                @csrf
                @method('post')
    
                <div class="card ">
                    <div class="card-header card-header-danger color-theme">
                        <h4 class="card-title">Product Details</h4>
                    </div>
                    <div class="card-body ">
                        @if (session('status_password'))
                        <div class="row">
                            <div class="col-sm-12">
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <i class="material-icons">close</i>
                                </button>
                                <span>{{ session('status_password') }}</span>
                            </div>
                            </div>
                        </div>
                        @endif
                        <div class="row">
                        <label class="col-sm-5 col-form-label" for="productCode">Citizen Code:</label>
                        <div class="col-sm-7">
                            <div class="form-group">
                            <input class="form-control" name="productCode" id="modal-productCode" type="text" placeholder="PRD0001" value="" required />
                            <input name="productId" id="modal-productId" type="text" value="" hidden />
                            </div>
                        </div>
                        </div>
                        <div class="row">
                        <label class="col-sm-5 col-form-label" for="title">Product Title:</label>
                        <div class="col-sm-7">
                            <div class="form-group">
                            <input class="form-control" name="title" id="modal-title" type="text" value="" required />
                            </div>
                        </div>
                        </div>
                        <div class="row">
                        <label class="col-sm-5 col-form-label" for="description">Product Description:</label>
                        <div class="col-sm-7">
                            <div class="form-group">
                            <input class="form-control" name="description" id="modal-description" type="text" value="" required />
                            </div>
                        </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-5 col-form-label" for="brand">Brand:</label>
                            <div class="col-sm-7">
                            <div class="form-group">
                                <input class="form-control" name="brand" id="modal-brand" type="text" value="" required />
                            </div>
                            </div>
                        </div>
                        {{-- <div class="row">
                            <label class="col-sm-5 col-form-label" for="partNumber">Part Number:</label>
                            <div class="col-sm-7">
                            <div class="form-group">
                                <input class="form-control" name="partNumber" id="partNumber" type="number" value="" required />
                            </div>
                            </div>
                        </div> --}}
                        <div class="row">
                            <label class="col-sm-5 col-form-label" for="category">Category</label>
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <select class="form-control" name="category" id="modal-category">
                                        @if (isset($categories))
                                            <option></option>

                                            @foreach ( $categories as $category )
                                                <option value={{$category->id}}>{{ $category->title }}</option>
                                            @endforeach
                                        @else
                                            <option>No Available Categories</option>
                                        @endif
                                    </select>
                                </div>
                            {{-- <div class="form-group">
                                <select class="form-control form-control-select" name="category" id="modal-category">
                                    @if (isset($categories))
                                        <option>Select Category</option>

                                        @foreach ( $categories as $category )
                                            <option value={{$category->id}}>{{ $category->title }}</option>
                                        @endforeach
                                    @else
                                        <option>No Available Categories</option>
                                    @endif
                                </select>
                            </div> --}}
                            </div>
                        </div>
                    </div>
                    <div class="card-footer ml-auto mr-auto">
                        <button type="submit" class="btn btn-danger">Save Product</button>
                    </div>
                </div>
            </form>

            <div class="card text-center">
                <div class="card-header">
                    Upload Products
                </div>
                <div class="card-body">
                    <form method="POST" id="upload-product-form" enctype="multipart/form-data">
                        @csrf
                        <div class="file btn btn-success">
                            Choose a file
                            <input type="file" name="fileUploadProduct" id="fileUploadProduct" style="position: absolute;font-size: 50px;opacity: 0;right: 0;top: 0;" onchange="productUpload()" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>
                        </div>
                    </form>
                    <div id="loader" hidden></div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="uploadInventory" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Upload Product Inventory</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="card " id="uploadButton-card">
                <div class="card-body text-center">
                    <form method="POST" id="upload-file-form" enctype="multipart/form-data">
                        @csrf
                        <div class="file btn btn-success" id="uploadButton">
                            Choose a file
                            <input type="file" name="fileUpload" id="fileUpload" style="position: absolute;font-size: 50px;opacity: 0;right: 0;top: 0;" onchange="inventoryUpload()" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>
                        </div>
                        <div id="loader" hidden></div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>