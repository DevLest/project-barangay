@extends('layouts.app', ['activePage' => 'new-transaction', 'titlePage' => 'Sales Transaction'])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header card-header-danger color-theme">
                </div>
                
                <div class="card-body">
                    <div class="row">
                        <div class="col-2">
                            <h4 class="card-title text-center">Product Code</h4>
                        </div>
                        <div class="col-6">
                            <form class="form form-horizontal form-product" method="post">
                                @csrf
                                <input type="hidden" name="idsales" value="{{ $idsales }}">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input id="productCode" type="text" class="form-control" name="productCode" autofocus required>
                                        <span class="input-group-btn">
                                            <button onclick="showProduct()" type="button" class="btn btn-info">...</button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <form class="form-cart forms-displayed" method="PATCH">
                            @csrf
                            <table class="table table-sales">
                                <thead>
                                    <tr>
                                    <th width="30">No</th>
                                    <th>Product Code</th>
                                    <th>Product Name</th>
                                    <th align="right">Price</th>
                                    <th>Total</th>
                                    <th>Discount</th>
                                    <th align="right">Sub Total</th>
                                    <th width="100">Action</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </form>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            {{-- <div id="total-amount" style="background: #dd4b39; color: #fff; font-size: 80px; text-align: center; height: 120px"></div> --}}
                            <div id="total-amount" class="color-theme display-1 text-center"></div>
                            <div id="total-amount-in-text" class="bg-info h2"></div>
                        </div>
                        <div class="col-md-4">
                            <form class="form form-horizontal form-sales forms-displayed" method="post" action="{{route('transaction.store')}}">
                                @csrf
                                <input type="hidden" name="idsales" value="{{ $idsales }}">
                                <input type="hidden" name="total" id="total">
                                <input type="hidden" name="totalitem" id="totalitem">
                                <input type="hidden" name="cash" id="cash">
                        
                                <div class="row">
                                    <label for="totalamount" class="col-md-4 control-label">Total</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="totalamount" readonly>
                                    </div>
                                </div>
                        
                                <div class="row">
                                    <label for="member" class="col-md-4 control-label">Member Code</label>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                        <input id="member" type="text" class="form-control" name="member" value="0">
                                        <span class="input-group-btn">
                                            <button onclick="showMember()" type="button" class="btn btn-info">...</button>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="row">
                                    <label for="discount" class="col-md-4 control-label">Discount</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="discount" id="discount" value="0" readonly>
                                    </div>
                                </div>
                        
                                <div class="row">
                                    <label for="amountDue" class="col-md-4 control-label">Amount Due</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="amountDue" readonly>
                                    </div>
                                </div>
                        
                                <div class="row">
                                    <label for="payment" class="col-md-4 control-label">Payment</label>
                                    <div class="col-md-8">
                                        <input type="number" class="form-control" value="0" name="payment" id="payment">
                                    </div>
                                </div>
                        
                                <div class="row">
                                    <label for="change" class="col-md-4 control-label">Change</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="change" value="0" readonly>
                                    </div>
                                </div>
                            </form>
                        </div>
            
                    </div>
                </div>
            </div>
        </div>
    </div>
  {{-- @include('sales_detail.product')
  @include('sales_detail.member') --}}

    <script type="text/javascript">
        var table;

        // $('.forms-displayed').on('keyup keypress', function(e) {
        //     var keyCode = e.keyCode || e.which;
        //     console.log(keyCode);
        //     if (keyCode === 13) { 
        //         e.preventDefault();
        //         return false;
        //     }
        // });

        $("#productCode").on('keyup', function (e) 
        {            
            if (e.key === 'Enter' || e.keyCode === 13)
            {
                console.log(1);
                addItem();
                // $.ajax({
                //     url : "{{ route('transaction.addItem') }}",
                //     type : "POST",
                //     data : $('.form-product').serialize(),
                //     success : function(data)
                //     {
                //         $('#productCode').val('').focus();
                //         table.ajax.reload(function()
                //         {
                //             loadForm($('#discount').val());
                //         });             
                //     },
                //     error : function()
                //     {
                //         alert("Unable to save data!");
                //     }   
                // });
            }
        });

        $(function()
        {
            $('.table-product').DataTable();

            table = $('.table-sales').DataTable(
                {
                    "dom" : 'Brt',
                    "bSort" : false,
                    "processing" : true,
                    "ajax" : {
                        "url" : "{{ route('transaction.data', $idsales) }}",
                        "type" : "GET"
                }
            }).on('draw.dt', function()
            {
                loadForm($('#discount').val());
            });

                $('.form-product').on('submit', function()
                {
                    return false;
                });

                // $('body').addClass('sidebar-collapse');

                // $('#productCode').change(function()
                // {
                //     addItem();
                // });

                $('.form-cart').submit(function()
                {
                    return false;
                });

                $('#member').change(function()
                {
                    selectMember($(this).val());
                });

                $('#payment').change(function()
                {
                    if($(this).val() == "") $(this).val(0).select();
                    loadForm($('#discount').val(), $(this).val());
                }).focus(function()
                {
                    $(this).select();
                });

                $('.save').click(function()
                {
                    $('.form-sales').submit();
                });

        });

        function addItem()
        {
            $.ajax({
                url : "{{ route('transaction.addItem') }}",
                type : "POST",
                data : $('.form-product').serialize(),
                success : function(data)
                {
                    $('#productCode').val('').focus();
                    table.ajax.reload(function()
                    {
                        loadForm($('#discount').val());
                    });             
                },
                error : function()
                {
                    alert("Unable to save data!");
                }   
            });
        }

        function showProduct()
        {
            $('#modal-product').modal('show');
        }

        function showMember()
        {
            $('#modal-member').modal('show');
        }

        function selectItem(productCode)
        {
            $('#productCode').val(productCode);
            $('#modal-product').modal('hide');
            addItem();
        }

        function changeCount(id)
        {
            $.ajax({
                url : "transaction/"+id,
                type : "POST",
                data : $('.form-cart').serialize(),
                success : function(data)
                {
                    $('#productCode').focus();
                    table.ajax.reload(function()
                    {
                        loadForm($('#discount').val());
                    });
                },
                error : function()
                {
                    alert("Unable to save data!");
                }   
            });
        }

        function selectMember(kode)
        {
            $('#modal-member').modal('hide');
            $('#member').val(kode);
            loadForm($('#discount').val());
            $('#payment').val(0).focus().select();
        }

        function deleteItem(id)
        {
            if(confirm("Are sure the data will be deleted?"))
            {
                $.ajax({
                    url : "transaction/"+id,
                    type : "POST",
                    data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                    success : function(data)
                    {
                        table.ajax.reload(function()
                        {
                            loadForm($('#discount').val());
                    }); 
                    },
                    error : function()
                    {
                        alert("Cannot delete data!");
                    }
                });
            }
        }

        function loadForm(discount=0, payment=0)
        {
            $('#total').val($('.total').text());
            $('#totalitem').val($('.totalitem').text());

            $.ajax({
                url : "transaction/loadform/"+discount+"/"+$('#total').val()+"/"+payment,
                type : "GET",
                dataType : 'JSON',
                success : function(data)
                {
                    $('#totalamount').val("₱ "+data.totalamount);
                    $('#amountDue').val("₱ "+data.amountDue);
                    $('#cash').val(data.cash);
                    $('#total-amount').html("<small>Amount:</small> ₱ "+data.amountDue);
                    $('#total-amount-in-text').text(data.paymentInText);
                    
                    $('#change').val("₱ "+data.change);

                    if($('#payment').val() != 0)
                    {
                        $('#total-amount').html("<small>Change:</small> ₱ "+data.change);
                        $('#total-amount-in-text').text(data.changeInText);
                    }
                },
                error: function(xhr, status, error) 
                {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                    alert("Cannot display data!");
                }
            });
        }
    </script>
    
  @endsection