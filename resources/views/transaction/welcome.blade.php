@extends('layouts.app', ['activePage' => 'transaction', 'titlePage' => 'Welcome'])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header card-header-danger color-theme">
                    {{-- <h4 class="card-title ">Point Of Sale</h4>
                    <p class="card-category"> Shop all you want</p> --}}
                </div>
                
                <div class="card-body">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1>Welcome {{Auth::User()->name}}</h1>
                        <a class="btn btn-success btn-lg" href="{{ route('transaction.create') }}">New Transaction</a>
                        <br><br><br>
                    </div>
                </div>
                <div class="table-responsive">
                </div>
                </div>
            </div>
        </div>
    </div>
    <script>
    </script>
@endsection