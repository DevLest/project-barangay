@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'home', 'titlePage' => " | FTS Devs"])

@section('content')
<div class="container" style="height: auto;">
  <div class="row justify-content-center">
      <div class="col-lg-7 col-md-8">
        <h1 class="text-white text-center">Welcome to {{env('STORE_TITLE')}}</h1><br>
        <img src="{{ asset('material') }}/img/{{env('STORE_LOGO')}}" class="img-fluid mx-auto d-block" style="max-width: 50%" alt="">
      </div>
  </div>
</div>
@endsection
