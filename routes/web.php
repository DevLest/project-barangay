<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {return redirect('home');})->middleware('auth');
Route::get('/welcome', function () {
    return view('welcome');
})->name('welcome');

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::group(['middleware' => 'auth'], function () {
	Route::get('table-list', function () {
		return view('pages.table_list');
	})->name('table');

	Route::get('typography', function () {
		return view('pages.typography');
	})->name('typography');

	Route::get('icons', function () {
		return view('pages.icons');
	})->name('icons');

	Route::get('map', function () {
		return view('pages.map');
	})->name('map');

	Route::get('notifications', function () {
		return view('pages.notifications');
	})->name('notifications');

	Route::get('rtl-support', function () {
		return view('pages.language');
	})->name('language');

	Route::get('upgrade', function () {
		return view('pages.upgrade');
	})->name('upgrade');
});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

	Route::resource('ci', 'CitizensController');

    Route::post('inventory-upload', 'ProductController@inventoryUpload')->name('product.inventory-upload');

	Route::resource('category', 'CategoryController');
	// Route::resource('transaction', 'TransactionController');
	Route::resource('household', 'HouseholdController');
	Route::resource('business', 'BusinessController');
	Route::resource('verification', 'VerificationController');
	Route::resource('payment', 'PaymentController');
	Route::resource('blotter', 'BlotterController');
    // Route::get('transaction/{id}/data', 'TransactionController@listData')->name('transaction.data');
    // Route::post('transaction/addItem', 'TransactionController@addItem')->name('transaction.addItem');
    // Route::get('transaction/loadform/{discount}/{total}/{received}', 'TransactionController@loadForm');
});


